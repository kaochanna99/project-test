import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import * as veevalidation from './plugins/vee-validate'
import * as baseComponent from './plugins/base-component'
import * as vuetoast from './plugins/vue-toast'
import * as filtersModule from './utils/filters'
import './assets/styles/main.scss'
Vue.config.productionTip = false

export const vm = new Vue({
  router,
  store,
  vuetify,
  veevalidation,
  baseComponent,
  vuetoast,
  filtersModule,
  render: h => h(App)
}).$mount('#app')
