import Vue from "vue";
import { upperFirst, camelCase } from "lodash";

const requireComponent = require.context(
  "../components",
  true,
  /(ui-)[\w-]+\.vue$/
);
requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName);
  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/ui\//, "").replace(/\.\w+$/, ""))
  );

  Vue.component(componentName, componentConfig.default || componentConfig);
});
