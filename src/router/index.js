import Vue from "vue";
import VueRouter from "vue-router";
import routes from "@/views/routes";
Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    const isAuthenticated = localStorage.getItem('user')
    if (isAuthenticated) {
      next();
     
    } else {
      next({ name: 'Login' });
    }
  }
    next();
});
export default router;
