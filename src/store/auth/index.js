import router from "@/router";
const SET_AUTH = "SET_AUTH";
const ERROR_MESSAGE = "ERROR_MESSAGE";
export default {
  namespace: true,
  state: {
    auth: {},
    error_message: ""
  },
  mutations: {
    [SET_AUTH](state, data) {
      state.auth = data;
      localStorage.setItem("user", data);
    },
    [ERROR_MESSAGE](state, data) {
      state.error_message = data;
    },
  },
  actions: {
    login({ commit }, data) {
      commit(ERROR_MESSAGE, "");
      const username = data.username;
      const password = data.password;
      if (username === "admin" && password === "Pa$$w0rd") {
        commit(SET_AUTH, JSON.stringify(data));
        router.push("/product");
      } else {
        commit(ERROR_MESSAGE, "Invalid username or password");
      }
    },
  },
  getters: {
    getauth: (state) => state.auth,
    error_message: (state) => state.error_message
  },
};
