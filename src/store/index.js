import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/auth/index.js'
import products from '@/store/products/index.js'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: auth,
    products: products
  }
})
