import { v4 as uuidv4 } from "uuid";
import { find ,map,filter} from "lodash";
const GET_PRODUCTS = "GET_PRODUCTS";
const SET_PRODUCT = "SET_PRODUCT";
const CHANGE_PRODUCT_VALUE = "CHANGE_PRODUCT_VALUE";
const RESET_PRODUCT_FORM = "RESET_PRODUCT_FORM";
const GET_ITEM = "GET_ITEM";
const UPDATE_PRODUCT = "UPDATE_PRODUCT";
const SET_NEW_PRODUCT = "SET_NEW_PRODUCT";
const DELETE_ITEM = "DELETE_ITEM";
const VIEW_PRODUCT = "VIEW_PRODUCT";
export default {
  namespace: true,
  state: {
    products: [],
    product_data: {
      id: "",
      name: "",
      quantity: "",
      price: "",
      description: "",
      image: "",
      file: null,
    },
    item_listing: [],
    view_product:{}
  },
  mutations: {
    [GET_PRODUCTS](state, data) {
      state.products = data;
      state.item_listing = state.products
    },
    [CHANGE_PRODUCT_VALUE](state, { key, value }) {
      state.product_data = { ...state.product_data, [key]: value };
    },
    [SET_PRODUCT](state, data) {
      state.products.push(data);
      state.item_listing = state.products
    },
    [RESET_PRODUCT_FORM](state) {
      state.product_data = {
        id: "",
        name: "",
        quantity: "",
        price: "",
        description: "",
        image: "",
        file: null,
      };
    },
    [GET_ITEM](state, id) {
     const products = find(state.products, function(item) {
        return item.id == id;
      });
      state.product_data = products
    },
    [UPDATE_PRODUCT](state, data) {
      state.products = map(state.products, function(item) {
        return item.id === data.id ? data : item
      })
      state.item_listing = state.products
    },
    [SET_NEW_PRODUCT](state,data){
      state.item_listing = data
    },
    [DELETE_ITEM](state,id){
      state.products = filter(state.products, function(item) {
        return (
          item.id !== id
        );
      });
      state.item_listing = state.products
    },
    [VIEW_PRODUCT](state,id){
     const views = find(state.products, function(item) {
        return item.id === id;
      });
      state.view_product = views
    },
  },
  actions: {
    getProducts({ commit, state }) {
      commit(GET_PRODUCTS, state.products);
    },
    createProduct({ commit,state }) {
      const alldata = { ...state.product_data, id: uuidv4() };
      commit(SET_PRODUCT, alldata);
      commit(RESET_PRODUCT_FORM);
    },
    getItem({ commit }, id) {
      commit(RESET_PRODUCT_FORM);
      commit(GET_ITEM, id);
    },
    updateProduct({ commit,state }) {
      commit(UPDATE_PRODUCT, state.product_data);
    },
    searchText({commit, state},data) {
      const products = filter(state.products, function(item) {
        return (
          item.name.toLowerCase().indexOf(data.toLowerCase()) != -1 ||
          item.description.toLowerCase().indexOf(data.toLowerCase()) != -1
        );
      });
      commit(SET_NEW_PRODUCT,products)
    },
    filterPrice({commit, state},val) {
      const price = val.split("-");
      const products = filter(state.products, function(item) {
        if (parseFloat(price[0]) < 100) {
          return (
            item.price >= parseFloat(price[0]) &&
            item.price <= parseFloat(price[1])
          );
         
        }else{
          return item.price > parseFloat(price[0]);
        }
        
      });
      commit(SET_NEW_PRODUCT,products)
    },
    filterQuantity({commit, state},quantity) {
      const products = filter(state.products, function(item) {
        return (
          item.quantity >= parseInt(quantity[0]) &&
          item.quantity <= parseInt(quantity[1])
        );
      });
      commit(SET_NEW_PRODUCT,products)
    },
    deleteProduct({commit},id){
      commit(DELETE_ITEM,id)
    }
  },
  getters: {
    products: (state) => state.products,
    product_data: (state) => state.product_data,
    item_listing: (state) => state.item_listing,
    view_product: (state) => state.view_product,
  },
};
