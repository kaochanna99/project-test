import Vue from 'vue';
const numeral = require('numeral');

Vue.filter('formatCurrency', function(value) {
    if(!value){
        return '';
    }
    return numeral(value).format('$0,0.00');
  })
  Vue.filter('formatNumber', function(value) {
    if(!value){
        return '';
    }
    return numeral(value).format('0,0');
  })
  Vue.filter('subString', function (value, limit) {
    if (value.length > limit) {
        value = value.substring(0, limit) + '...';
    }

    return value
})