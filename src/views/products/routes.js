import { default as ProductListing } from "./index.vue";
import { default as CreateProduct } from "./create.vue";
import { default as EditProduct } from "./edit.vue";
import { default as Dashboard } from "../layouts/dashboard.vue";
export default [
  {
    path: "/product",
    component: Dashboard,
    meta: {
        auth: true,
      },
    children: [
      {
        path: "/",
        name: "poroduct_listing",
        component: ProductListing,
        meta: {
          auth: true,
        },
      },
      {
        path: "create",
        name: "create_product",
        component: CreateProduct,
        meta: {
          auth: true,
        },
      },
      {
        path: "edit/:id",
        name: "edit_product",
        component: EditProduct,
        meta: {
          auth: true,
        },
      },
    ],
  },
];
