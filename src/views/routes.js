import { default as products } from '@/views/products/routes'
import { default as login } from '@/views/login/routes'

export default [
    ...products,
    ...login
]